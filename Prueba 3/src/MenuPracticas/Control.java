/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MenuPracticas;

import javax.swing.JOptionPane;

/**
 *
 * @author ACER
 */
public class Control {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String[] Practica={"Saludador", "Generador_Numero", "Imitador", "Menu", "MiniEncuesta", "Peliculas"};
        String[] YN={"SI","NO"};
        String Opcion;
        do{
        Opcion = (String)JOptionPane.showInputDialog(null, "Elija la practica a mostrar", "Menu de Practicas", JOptionPane.QUESTION_MESSAGE, null, Practica, Practica[0]);
        
        switch(Opcion){
            case "Saludador":
                Practicas.Saludador p0 = new Practicas.Saludador();
                while(p0.state()==1);
                break;
                
            case "Generador_Numero":
                Practicas.Generador_Num p1 = new Practicas.Generador_Num();
                while(p1.state()==1);
                break;
                
            case "Imitador":
                Practicas.Imitador p2 = new Practicas.Imitador();
                while(p2.state()==1);
                break;
                
            case "Menu":
                Practicas.Menu p3 = new Practicas.Menu();
                while(p3.state()==1);
                break;
                
            case "MiniEncuesta":
                Practicas.MiniEncuesta p4= new Practicas.MiniEncuesta();
                while(p4.state()==1);
                break;
                
            case "Peliculas":
                Practicas.Peliculas p5 = new Practicas.Peliculas();
                while(p5.state()==1);
                break;
            /*    
            case "1erCaso_Especial: MiniEncuesta(mysql)":
                PracticasAdaptadasMyslql.MiniEncuesta p6 = new PracticasAdaptadasMyslql.MiniEncuesta();
                while(p6.state()==1);
                break;
                
            case "2doCaso_Especial: MiniEncuestaVisualizar(mysql)":                
                PracticasAdaptadasMyslql.MiniEncuestaVisualizar p7 = new PracticasAdaptadasMyslql.MiniEncuestaVisualizar();
                while(p7.state()==1);
                break;*/
        }
        Opcion=(String)JOptionPane.showInputDialog(null, "¿Desea ver otra practica", "", JOptionPane.QUESTION_MESSAGE, null, YN, YN[0]);
        }while(Opcion.equals("SI"));
        //se vinculan todos los archivos de netbeans
    }
    
}
