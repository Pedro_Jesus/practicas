package CHATmySql;

import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Scanner;
import java.net.SocketException;

/**
 *
 * @author gmendez
 */
public class Cliente implements Runnable{
    Chatear c;
    BufferedReader in;
    PrintWriter    out;
    
    Socket cnx;
    public Cliente(String ip, int port, Chatear msm){
        this.c=msm;
        this.Ip=ip;
        this.port=port;
    }
    /*
    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run(){
                (new Cliente("127.0.0.1",4444, null)).run();
            }
        });
    }*/


    @Override
    public void run() {
        String respuesta="", comando = "";
        Conexion hilo;
        Scanner consola = new Scanner(System.in);
        try {
            this.cnx = new Socket(this.Ip, this.port);
            
            in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
            out = new PrintWriter(cnx.getOutputStream(),true);
            
            hilo = new Conexion(in, c);
            hilo.start(); //Hilo encargado de lecturas del servidor
            boolean ie=false;
            while (!comando.toLowerCase().contains("salir")){                
                //while(!c.ESTATUS){}                
                if(!ie && c.getESTATUS()){                                  
                    out.println(c.nomUser());
                    out.println(c.passUser());
                    tiempo=false;                    
                }else if(c.envio() && c.getESTATUS()){
                    out.println(c.jtextEnviar());
                }
                while(!tiempo){}
                if(valid)ie=true;                
            }/*
            while (!comando.toLowerCase().contains("salir")){
                System.out.println("eje");
               comando = consola.nextLine();              
                out.println(comando); 
            }*/
            
            hilo.ejecutar = false;
            Thread.sleep(2000);
            
            this.cnx.close();
            
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }      
    }
    
    class Conexion extends Thread {
        public boolean ejecutar = true;
        BufferedReader in;
        Chatear c;
        
        public Conexion(BufferedReader in, Chatear msm){
            c=msm;
            this.in = in;
        }
        
        @Override
        public void run() {
            String respuesta = "";
            while(ejecutar){
                try { 
                    respuesta = in.readLine();                    
                    //if (respuesta!=null)
                    if(!respuesta.equals("")){
                    c.actualizar(respuesta);
                    if (!valid)valid(respuesta);                       
                    }
                } catch (SocketException | NullPointerException ex2){
                    System.exit(0);
                } catch (IOException ex) {
                    Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
                }               
            }
        }   
    }
    public void valid(String respuesta){
        switch(respuesta){
            case "BOOT: usuario no registrado favor de ingresar uno valido":
                //c.ESTATUS=false;
                c.setESTATUS(false);
                c.actualizar_parametros(2);
                break;
                
            case "BOOT: contraseña invalida, ingresela de nuevo":
                //c.ESTATUS=false;
                c.setESTATUS(false);
                c.actualizar_parametros(1);
                break;
                
            default : valid=true;
                c.chatActivo(true);
                break;
        }
        tiempo=true;
    }
    private boolean tiempo=false;
    private boolean valid=false;
    private int port;
    private String Ip;
}
