package CHATmySql;

import java.net.*;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author gmendez
 */
public class Servidor implements Runnable{
    Chatear c;
    boolean STATE=false;
    ArrayList<Conexion> conexiones;
    ServerSocket   ss;
    /*
    String [][] usuarios = {{"hugo",  "123"},
                            {"paco",  "345"},
                            {"luis",  "890"},
                            {"donald","678"}};*/
    public Servidor(int port, Chatear c){        
        this.port=port;
        this.c=c;
    }
    /*
    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                (new Servidor(4444,null)).run();
            }
        });
    } */
    
    public void run() {
        this.conexiones = new ArrayList<>();
        Socket socket;
        Conexion cnx;
        
        try {
            ss = new ServerSocket(port);
            c.actualizarMSM("Servidor iniciado, en espera de conexiones");
            //System.out.println("Servidor iniciado, en espera de conexiones");
            STATE=true;
            while (true){             
                socket = ss.accept();
                cnx = new Conexion(this, socket, conexiones.size(), c);
                conexiones.add(cnx);
                cnx.start();                
            }            
                       
        } catch (IOException ex) {
            STATE=true;
            //System.out.println("servidor ocupado");
            //Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }            
    }
    
    // Broadcasting
    private void difundir(String id, String mensaje) {
        Conexion hilo;
        for (int i = 0; i < this.conexiones.size(); i++){
            hilo = this.conexiones.get(i);
            if (hilo.cnx.isConnected()){
                if (!id.equals(hilo.id)){
                    hilo.enviar(mensaje);
                }
            }
        }//To change body of generated methods, choose Tools | Templates.
    }
    
    class Conexion extends Thread {
        int estado=1;
        Chatear c;
        BufferedReader in;
        PrintWriter    out;        
        Socket cnx;
        Servidor padre;
        int numCnx = -1;
        String id = "";
        
        public final int SIN_USER   = 0;
        public final int USER_IDENT = 1;
        public final int PASS_PDTE  = 2;
        public final int PASS_OK    = 3;
        public final int CHAT       = 4;
                
        public Conexion(Servidor padre, Socket socket, int num, Chatear c){
            this.c=c;
            this.cnx = socket;
            this.padre = padre;
            this.numCnx = num;
            this.id = socket.getInetAddress().getHostAddress()+num;
            //System.out.println(socket.getInetAddress().getHostAddress());
        }
        
        @Override
        public void run() {
            String linea="", user="", pass="", mensaje="";
            estado = USER_IDENT;
            int usr = -1;
            
            try {
                in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
                out = new PrintWriter(cnx.getOutputStream(),true);
                /*
                System.out.printf("Aceptando conexion desde %s\n",
                        cnx.getInetAddress().getHostAddress());*/
                c.actualizarMSM("Aceptando conexion desde \n"+cnx.getInetAddress().getHostAddress());
                conexion();
                while(!mensaje.toLowerCase().equals("salir")){                    
                    switch (estado){
                        case SIN_USER:
                            out.println("BOOT: usuario no registrado favor de ingresar uno valido\n");
                            estado = USER_IDENT;
                            break;
                        case USER_IDENT:
                            user = in.readLine();
                            pass = in.readLine();
                            buscar(user,pass);
                            /*boolean found = false;
                            for (int i=0; i < usuarios.length; i++){
                                if (user.equals(usuarios[i][0])){
                                    found = true;
                                    usr = i;
                                }
                            }                            
                            
                            if (!found){
                                estado = SIN_USER;
                            } else {
                                estado = PASS_PDTE;
                            }*/
                            break;
                        case PASS_PDTE:
                            out.println("BOOT: contraseña invalida, ingresela de nuevo\n");
                            user = in.readLine();
                            pass = in.readLine();
                            buscar(user,pass);
                            /*if (pass.equals(usuarios[usr][1])){
                                estado = PASS_OK;
                            }*/
                            break;
                        case PASS_OK:
                            out.println("Autenticado!\n");
                            estado = CHAT;
                            break;
                        case CHAT:
                            mensaje = in.readLine();
                            /*System.out.printf("%s - %s\n",
                                        cnx.getInetAddress().getHostAddress(),
                                        user + ":" +mensaje);*/
                            out.println(user+" : "+mensaje);//for me
                            this.padre.difundir(this.id, user+" : "+mensaje);
                            break;
                    }                        
                }                
                this.cnx.close();
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }            
        }

        private void enviar(String mensaje) {
            this.out.println(mensaje); //To change body of generated methods, choose Tools | Templates.
        }
        public Connection conexion() {
            try {
                //Class.forName("com.mysql.jdbc.Driver");
                //conectar=DriverManager.getConnection("jdbc:mysql://localhost/bodega","root","root");
                con = DriverManager.getConnection(url, "root", "root");
                //System.out.println("conectado a BD");
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            return con;
        }

        public void buscar(String nom, String pass) {
            Statement stmt;
            String sQuery;
            ResultSet rset;

            sQuery = "SELECT NOMBRE,PASSWORD FROM usuarios";
            //System.out.println(sQuery);

            try {
                stmt = con.createStatement();
                rset = stmt.executeQuery(sQuery);
                while (rset.next()) {
                    //tabla.addRow(new Object[]{rset.getString("NOMBRE"),rset.getString("PASSWORD")});
                    if (nom.equals(rset.getString("NOMBRE"))) {
                        if (pass.equals(rset.getString("PASSWORD"))) {                            
                            estado=PASS_OK;//éxito
                            break;
                        }else estado=PASS_PDTE;//contraseña incorrecta
                    }else estado=SIN_USER;//no se encontro el usuario;
                }
            } catch (SQLException ex) {
                Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    } 
    private int port;
    private Connection con = null;
    private final String url = "jdbc:mysql://localhost:3306/chat" + "?useUnicode=true&use"
                + "JDBCCompilantTimezoneShift=true&useLegacyDatetimeCode=false&"
                + "serverTimezone=UTC";
}
