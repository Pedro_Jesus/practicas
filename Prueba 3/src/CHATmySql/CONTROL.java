package CHATmySql;


/**
 *
 * @author Pedro
 */
public class CONTROL{  
  
    public static void main(String args[]){
        
        Chatear msm=new Chatear();//                                            jFrame de inicio
        msm.setVisible(true);       
              
        while(!msm.getESTATUS());//                                                 espera de parametros          
                
        Servidor ser= new Servidor(msm.getPort(), msm);//                       instancia del servidor
        Cliente cli= new Cliente(msm.getIp(), msm.getPort(), msm);//            instancia del cliente
        
        Thread s= new Thread(ser);//                                            ejecucion del hilo server
        Thread c= new Thread(cli);//                                            ejecucion del hilo 
        
        s.start();//                                                            inicio del server
        while(!ser.STATE){}//                                                    en caso de que ya exista un server, no se inicio ningun otro
        c.start();//                                                            inicio del cliente               
        
    }
}
