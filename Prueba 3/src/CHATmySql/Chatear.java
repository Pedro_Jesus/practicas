package CHATmySql;

import java.awt.Graphics;
import java.awt.Image;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author Pedro
 */
public class Chatear extends javax.swing.JFrame{
    /**
     * Creates new form NewJFrame
     */
    public Chatear() { 
        //this.setIconImage(new ImageIcon(getClass().getResource("/CHATmySql/IMG/Icon1.png")).getImage());
        this.setContentPane(fondo);        
        initComponents();
        
        chatActivo(false);
        try {
            InetAddress address = InetAddress.getLocalHost();
            //System.out.println("IP Local :"+address.getHostAddress());
            newIp.setText(address.getHostAddress());
        } catch (UnknownHostException ex) {
            Logger.getLogger(Chatear.class.getName()).log(Level.SEVERE, null, ex);
            newIp.setText("127.168.100.1");
        }
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        newIp = new javax.swing.JTextField();
        newPort = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        nomUser = new javax.swing.JTextField();
        passUser = new javax.swing.JTextField();
        param = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        textos = new javax.swing.JTextArea();
        enviar = new javax.swing.JTextField();
        out = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Mensajeria");
        setIconImage(new ImageIcon(getClass().getResource("/CHATmySql/IMG/Icon1.png")).getImage());
        setResizable(false);

        jPanel1.setOpaque(false);

        newIp.setEditable(false);

        newPort.setText("4444");

        jLabel2.setText("Direccion IP:  ");

        jLabel3.setText("Puerto de enlace: ");

        jLabel4.setText("Usuario: ");

        jLabel5.setText("Constraseña: ");

        nomUser.setText("Pedro");

        passUser.setText("123");

        param.setText("Conectar");
        param.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paramActionPerformed(evt);
            }
        });

        jLabel1.setText("...");

        jButton1.setText("Salir");
        jButton1.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(237, 237, 237)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(passUser, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(18, 18, 18)
                                .addComponent(nomUser, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(newIp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(10, 10, 10)
                                        .addComponent(jLabel3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(newPort, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(param, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(nomUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(passUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(newIp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(newPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(param))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        textos.setEditable(false);
        textos.setColumns(20);
        textos.setRows(5);
        textos.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        textos.setOpaque(false);
        jScrollPane1.setViewportView(textos);

        out.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CHATmySql/IMG/Icon0.png"))); // NOI18N
        out.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 454, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(enviar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(out, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(enviar)
                    .addComponent(out, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void paramActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paramActionPerformed
        nomUser.setEditable(false);                                             //envio de parametros
        passUser.setEditable(false);
        newIp.setEditable(false);
        int e=Integer.parseInt(newPort.getText());
        if(e>999){
        jLabel1.setText("...");
        newPort.setEditable(false);
        ESTATUS=true;        
        }else{
            jLabel1.setText("Ingrese un puerto invalido, por ejemplo: 3333 ó 4444");
        }
        
    }//GEN-LAST:event_paramActionPerformed

    private void outActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_outActionPerformed
        envio=true;                                                             //envio de mesajes
    }//GEN-LAST:event_outActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        enviar.setText("salir");                                                //salir
        envio=true;
        this.dispose();
        System.exit(0);
    }//GEN-LAST:event_jButton1ActionPerformed
    public void actualizar(String texto){//                                     Actualiza los mensajes
        txts+=texto+"\n";
        textos.setText(txts);
    }
    public void actualizarMSM(String texto){//                                  Actualiza la etiqueta de texto
        jLabel1.setText(texto);
    }
    public String getIp(){//                                           Retorna la Ip ingresado
        return this.newIp.getText();
    }
    public int getPort(){//                                                     Retorna el puerto ingresado
        return Integer.parseInt(this.newPort.getText());
    }
    public String nomUser(){//                                                  retorna el nombre ingresado
        return this.nomUser.getText();
    }
    public String passUser(){//                                                 Retorna el password
        return this.passUser.getText();
    }    
    public void chatActivo(boolean estado){
        enviar.setEditable(estado);
        out.setEnabled(estado);
    }
    public void actualizar_parametros(int i){//                                 Actualiza los parametros(nom, pass)
        if(i==2){
            nomUser.setText("");
            passUser.setText("");
            nomUser.setEditable(true);
            passUser.setEditable(true);            
        }else{
            passUser.setText("");
            passUser.setEditable(true); 
        }
    }
    public String jtextEnviar(){//                                              retorna el texto a enviar y limpia el area a escribir
        String respaldo=this.enviar.getText();
        enviar.setText("");
        this.envio=false;
        return respaldo;
    }
    public boolean envio(){//                                                   alerta sobre un nuevo texto
        return envio;
    }
    public void setESTATUS(boolean t){//                                        referencia a modificar el estado de casillas de los parametros
        this.ESTATUS=t;
    }
    public boolean getESTATUS(){//                                              referencia a que estan o no las casillas de los parametros
        return this.ESTATUS;
    }
    private Fondos fondo= new Fondos("/CHATmySql/IMG/Fondo1.png");
    private boolean envio=false;
    private String txts="";
    private String msm="";
    private boolean ESTATUS=false;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField enviar;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField newIp;
    private javax.swing.JTextField newPort;
    private javax.swing.JTextField nomUser;
    private javax.swing.JButton out;
    private javax.swing.JButton param;
    private javax.swing.JTextField passUser;
    private javax.swing.JTextArea textos;
    // End of variables declaration//GEN-END:variables
    private class Fondos extends javax.swing.JPanel{
        private Image imagen;
        private String ruta;
    
        public Fondos(String ruta){
            this.ruta=ruta;
        }
        @Override
        public void paint(Graphics g){        
            imagen = new ImageIcon(getClass().getResource(ruta)).getImage();
            g.drawImage(imagen, 0, 0, getWidth(), getHeight(), this);
            setOpaque(false);
            super.paint(g);
        }    
    }
}
