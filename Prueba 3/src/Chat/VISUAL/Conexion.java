package VISUAL;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
/**
 *
 * @author Pedro
 */
public class Conexion extends Thread{        
        CHATEAR nc;
        BufferedReader in;
        PrintWriter out;        
        Socket cnx;
        String id = ""; 

    public Conexion(Socket socket, int num,CHATEAR NC){
            this.nc=NC;            
            this.cnx = socket;
            this.id = socket.getInetAddress().getHostAddress()+num;
        }
    @Override
        public void run() {            
            try {
                in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
                out = new PrintWriter(cnx.getOutputStream(),true);                         
                        nc.actualizar("Aceptando conexion desde \n" + cnx.getInetAddress().getHostAddress());
                        
                /////
                out.println(nc.nom()+": Bienvenido");
                String mensaje;
                while(true){
                mensaje = in.readLine();
                nc.actualizar(mensaje);
                }
                //this.cnx.close();
            } catch (IOException ex) {
                System.out.println("error");
                //Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }            
        }
        
        public void escribir(String msm){
            out.println(msm);
            nc.actualizar(msm);            
        }
}
