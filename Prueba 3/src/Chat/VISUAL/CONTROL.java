package VISUAL;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class CONTROL{  
  
    public static void main(String args[]){
        
        
        
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LOGIN.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LOGIN.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LOGIN.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LOGIN.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }//</editor-fold>//</editor-fold>
        /*java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new LOGIN().setVisible(true);
                
            }});*/
        
        LOGIN l= new LOGIN();
        CHATEAR nc;
        int sc=l.getTipoUsuario(),portNumber=0;        
        String fromServer="", hostName="";       
                
        l.setVisible(true);
        while(sc==0){ 
            sc=l.getTipoUsuario();
            //System.out.println("Esperando respuesta del tipo de usuario");
        }//<editor-fold>
        hostName=l.getIp();
        if(hostName.equals(""))hostName="127.0.0.1";
        portNumber=l.getPort();
        if(portNumber<1000)portNumber=4444;
        System.out.println(hostName+" "+portNumber);//</editor-fold>
        nc = new CHATEAR(l.getTP(), l.getNombre());
        l.dispose();
        //nc.setVisible(true);
        if(sc==1){                                                              //caso server 
            //nc.setDat("Server", "nombreServer");
            //nc.setVisible(true);            
        //<editor-fold>
         Socket socket;
         ServerSocket   ss;
         Conexion cnx1;
         ArrayList<Conexion> conexiones;
        conexiones = new ArrayList<>();  
        try {
            ss = new ServerSocket(portNumber);
            nc.actualizar("Servidor iniciado, en espera de conexiones");
            //while (true){ 
                nc.setVisible(true);
                socket = ss.accept();
                cnx1=new Conexion(socket,conexiones.size(),nc);
                nc.escs=cnx1;
                conexiones.add(cnx1);               
                cnx1.start();//enlaze                
            //}            
                       
        } catch (IOException ex) {
                System.out.println("error, puerto ocupado");
//Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }  //</editor-fold>
        }else {                                                                  //caso cliente
            //nc.setDat("Cliente", "nombreCliente");
            //nc.setVisible(true);
        //<editor-fold>        
        try (
            Socket kkSocket = new Socket(hostName, portNumber);
            PrintWriter out = new PrintWriter(kkSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(
                new InputStreamReader(kkSocket.getInputStream()));
        ) {
            //BufferedReader stdIn =
                //new BufferedReader(new InputStreamReader(System.in));
            //outt=out;
            nc.escc=out;
            nc.setVisible(true);
            while(true){
            fromServer = in.readLine();   
            nc.actualizar(fromServer);
            }//</editor-fold>          
        //<editor-fold> 
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("El servidor se ah detenido (" +
                hostName+")");
            System.exit(1);
        }
        //</editor-fold>             
        }        
    }
}
